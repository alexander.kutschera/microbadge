#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif
#define PIN 4
#define NUMPIXELS 3
#define BRIGHTNESS 20

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

#include <OneWire.h>
#define DS18B20 2                  // Datenpin Temperatursensor
OneWire Sensor(DS18B20);
byte data[12], gain, i;
int16_t raw;
int celsius;


const int LEDpin = 0;


int R = 0;
int G = 0;
int B = 0;

void setup() {
  // put your setup code here, to run once:
  pinMode(LEDpin, OUTPUT);
  #if defined (__AVR_ATtiny85__)
    if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
  #endif
  // End of trinket special code
  pixels.begin(); // This initializes the NeoPixel library.
}

void loop(){
  readDS18B20();                   // Temperatur einlesen
  // Umrechnen der vom Temperatursensor gelesenen Daten für den Digital/Analog Wandler
  raw = (data[1] << 8) | data[0];  // Rohdaten von DS18B20 in int umrechnen
  if (bitRead(raw, 15)) raw = 0;  // bei negativen Temperaturmesswerten auf 0 setzen
  celsius = raw *10 / 32;          // für Ausgabe von 10mV/GrdC umrechnen

  R = celsius*7;
  B = 220 - celsius*7;
  pixels.setPixelColor(0, pixels.Color(R, G, B));
  pixels.setPixelColor(1, pixels.Color(R, G, B));
  pixels.setPixelColor(2, pixels.Color(R, G, B));
  pixels.show(); // This sends the updated pixel color to the hardware
  
}

void readDS18B20 (void){
  Sensor.reset();       // reset one wire buss
  Sensor.skip();        // select only device
  Sensor.write(0x44);   // start conversion

  delay(1000);          // wait for the conversion

  Sensor.reset();
  Sensor.skip();
  Sensor.write(0xBE);   // Read Scratchpad
    for ( i = 0; i < 9; i++) {       // 9 bytes
      data[i] = Sensor.read();
    }    
}
