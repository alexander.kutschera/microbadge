#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif
#define PIN 4
#define NUMPIXELS 3
#define BRIGHTNESS 20

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

const int LEDpin = 0;

int R = 0;
int G = 0;
int B = 0;

void setup() {
  // put your setup code here, to run once:
  pinMode(LEDpin, OUTPUT);
  #if defined (__AVR_ATtiny85__)
    if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
  #endif
  // End of trinket special code
  pixels.begin(); // This initializes the NeoPixel library.
}

void loop() {
  // put your main code here, to run repeatedly:
  for(int i=0;i<25;i++){
    digitalWrite(LEDpin, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(200);                       // wait for a second
    digitalWrite(LEDpin, LOW);    // turn the LED off by making the voltage LOW
    delay(200);
    // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
    R = i*10;
    G = (25-i)*10;
    B = 20;
    pixels.setPixelColor(0, pixels.Color(R, G, B)); // Moderately bright green color.
    pixels.setPixelColor(1, pixels.Color(R, G, B));
    pixels.setPixelColor(2, pixels.Color(R, G, B));
    pixels.show(); // This sends the updated pixel color to the hardware.    
    
  }
  for(int i=0;i<25;i++){
    digitalWrite(LEDpin, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(200);                       // wait for a second
    digitalWrite(LEDpin, LOW);    // turn the LED off by making the voltage LOW
    delay(200);
    // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
    R = (25-i)*10;
    G = i*10;
    B = 20;
    pixels.setPixelColor(0, pixels.Color(R, G, B)); // Moderately bright green color.
    pixels.setPixelColor(1, pixels.Color(R, G, B));
    pixels.setPixelColor(2, pixels.Color(R, G, B));
    pixels.show(); // This sends the updated pixel color to the hardware.    
    
  }
}
